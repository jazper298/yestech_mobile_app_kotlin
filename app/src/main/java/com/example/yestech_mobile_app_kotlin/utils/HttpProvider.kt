package com.example.yestech_mobile_app_kotlin.utils

import android.content.Context
import com.loopj.android.http.*
import cz.msebera.android.httpclient.entity.StringEntity

class HttpProvider {

    private val BASE_URL = "http://192.168.1.4/learning_app/controllerClass/"

    private val client = AsyncHttpClient()
    private val syncHttpClient = SyncHttpClient()

    fun post(context: Context, url: String, entity: StringEntity, responseHandler: AsyncHttpResponseHandler) {
        client.addHeader("Authorization", "Token " + UserSessionStudent().getToken(context))
        client.addHeader("Content-Type", "application/x-www-form-urlencoded")
        client.addHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)")
        client.post(context, getAbsoluteUrl(url), entity, null, responseHandler)
    }

    operator fun get(context: Context, url: String, params: RequestParams, jsonHttpResponseHandler: JsonHttpResponseHandler) {
        client.addHeader("Authorization", "Token " + UserSessionStudent().getToken(context))
        client.addHeader("Content-Type", "application/x-www-form-urlencoded")
        client.setMaxRetriesAndTimeout(2, 5)
        client.get(context, getAbsoluteUrl(url), params, jsonHttpResponseHandler)
    }

    fun postRegister(context: Context, url: String, entity: StringEntity, responseHandler: AsyncHttpResponseHandler) {
        client.addHeader("Content-Type", "application/x-www-form-urlencoded")
        client.post(context, getAbsoluteUrl(url), entity, null, responseHandler)
    }

    fun post(context: Context, url: String, entity: StringEntity, possibleLongOperation: Boolean, responseHandler: AsyncHttpResponseHandler) {
        client.addHeader("Authorization", "Token " + UserSessionStudent().getToken(context))
        client.addHeader("Content-Type", "application/json;charset=UTF-8")
        client.addHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)")

        if (possibleLongOperation) {
            client.connectTimeout = 50000
            client.setTimeout(50000)
            client.responseTimeout = 50000
        }

        client.post(context, BASE_URL + url, entity, null, responseHandler)
    }

    fun postSync(
        context: Context,
        url: String,
        entity: StringEntity,
        possibleLongOperation: Boolean,
        responseHandler: AsyncHttpResponseHandler
    ) {
        syncHttpClient.addHeader("Authorization", "Token " + UserSessionStudent().getToken(context))
        syncHttpClient.addHeader("Content-Type", "application/json;charset=UTF-8")
        syncHttpClient.addHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)")

        if (possibleLongOperation) {
            syncHttpClient.connectTimeout = 50000
            syncHttpClient.setTimeout(50000)
            syncHttpClient.responseTimeout = 50000
        }

        syncHttpClient.post(context, getAbsoluteUrl(url), entity, null, responseHandler)
    }

    fun postLogin(ctx: Context, url: String, params: RequestParams, responseHandler: AsyncHttpResponseHandler) {
        /* Fixed Login problem where second login attempt after logout changes content type of data */
        /* Problem - Can't Login after logout from previous account */
        /* Cause - Content type is changed or has been added other type due to other http call (read_inventory) */
        /* Solution - Create new AssyncHttp Instance to ensure no previous content type or header has been set */

        val asyncHttpClient = AsyncHttpClient()
        asyncHttpClient.post(ctx, getAbsoluteUrl(url), params, responseHandler)
    }

    private fun getAbsoluteUrl(relativeUrl: String): String {
        return BASE_URL + relativeUrl
    }
}