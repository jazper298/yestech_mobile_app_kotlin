package com.example.yestech_mobile_app_kotlin.utils

import android.content.Context
import android.preference.PreferenceManager
import com.google.gson.annotations.SerializedName

class UserSessionStudent{
    @SerializedName("stud_id")
    private var studID: String? = null
    @SerializedName("stud_token")
    private var studToken: String? = null
    @SerializedName("stud_email_address")
    private var studEmail: String? = null
    @SerializedName("stud_firstname")
    private var studFirstname: String? = null
    @SerializedName("stud_lastname")
    private var studLastname: String? = null
    @SerializedName("stud_middlename")
    private var studMiddlename: String? = null
    @SerializedName("stud_suffixes")
    private var studSuffixes: String? = null
    @SerializedName("stud_gender")
    private var studGender: String? = null
    @SerializedName("stud_contact_number")
    private var studContactNumber: String? = null
    @SerializedName("stud_image")
    private var studImage: String? = null

//    internal fun UserSessionStudent(): ??? {
//
//    }

    fun getStudID(): String? {
        return studID
    }

    fun setStudID(studID: String) {
        this.studID = studID
    }

    fun getStudToken(): String? {
        return studToken
    }

    fun setStudToken(studToken: String) {
        this.studToken = studToken
    }

    fun getStudEmail(): String? {
        return studEmail
    }

    fun setStudEmail(studEmail: String) {
        this.studEmail = studEmail
    }

    fun getStudFirstname(): String? {
        return studFirstname
    }

    fun setStudFirstname(studFirstname: String) {
        this.studFirstname = studFirstname
    }

    fun getStudLastname(): String? {
        return studLastname
    }

    fun setStudLastname(studLastname: String) {
        this.studLastname = studLastname
    }

    fun getStudMiddlename(): String? {
        return studMiddlename
    }

    fun setStudMiddlename(studMiddlename: String) {
        this.studMiddlename = studMiddlename
    }

    fun getStudSuffixes(): String? {
        return studSuffixes
    }

    fun setStudSuffixes(studSuffixes: String) {
        this.studSuffixes = studSuffixes
    }

    fun getStudGender(): String? {
        return studGender
    }

    fun setStudGender(studGender: String) {
        this.studGender = studGender
    }

    fun getStudContactNumber(): String? {
        return studContactNumber
    }

    fun setStudContactNumber(studContactNumber: String) {
        this.studContactNumber = studContactNumber
    }

    fun getStudImage(): String? {
        return studImage
    }

    fun setStudImage(studImage: String) {
        this.studImage = studImage
    }

    fun getID(context: Context): String? {
        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        return settings.getString("STUD_ID", "")
    }

    fun getToken(context: Context): String? {
        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        return settings.getString("STUD_TOKEN", "")
    }

    fun getEmail(context: Context): String? {
        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        return settings.getString("STUD_EMAIL", "")
    }

    fun getFirstname(context: Context): String? {
        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        return settings.getString("STUD_FIRSTNAME", "")
    }

    fun getLastname(context: Context): String? {
        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        return settings.getString("STUD_LASTNAME", "")
    }

    fun getMiddlename(context: Context): String? {
        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        return settings.getString("STUD_MIDDLENAME", "")
    }

    fun getSuffix(context: Context): String? {
        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        return settings.getString("STUD_SUFFIX", "")
    }

    fun getGender(context: Context): String? {
        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        return settings.getString("STUD_GENDER", "")
    }

    fun getContactNumber(context: Context): String? {
        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        return settings.getString("STUD_CONTACTNUMBER", "")
    }

    fun getImage(context: Context): String? {
        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        return settings.getString("STUD_IMAGE", "")
    }

    fun saveUserSession(context: Context): Boolean {
        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = settings.edit()
        editor.putString("STUD_ID", studID)
        editor.putString("STUD_TOKEN", studToken)
        editor.putString("STUD_EMAIL", studEmail)
        editor.putString("STUD_FIRSTNAME", studFirstname)
        editor.putString("STUD_LASTNAME", studLastname)
        editor.putString("STUD_MIDDLENAME", studMiddlename)
        editor.putString("STUD_SUFFIX", studSuffixes)
        editor.putString("STUD_GENDER", studGender)
        editor.putString("STUD_CONTACTNUMBER", studContactNumber)
        editor.putString("STUD_IMAGE", studImage)
        return editor.commit()
    }

    fun clearSession(context: Context): Boolean {
        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = settings.edit()
        editor.clear()
        return editor.commit()
    }
}