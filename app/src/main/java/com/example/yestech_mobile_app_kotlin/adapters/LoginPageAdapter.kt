package com.example.yestech_mobile_app_kotlin.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.widget.DialogTitle

class LoginPageAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager){

    private val fragmentList : MutableList<Fragment> = ArrayList()
    private val titleList : MutableList<String> = ArrayList()
    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    override fun getCount(): Int {
        return fragmentList.size
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun addFragment(fragment: Fragment,title: String){
        fragmentList.add(fragment)
        titleList.add(title)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titleList[position]
    }



}