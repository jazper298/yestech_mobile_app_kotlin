package com.example.yestech_mobile_app_kotlin

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.FragmentManager
import android.view.MenuItem
import com.example.yestech_mobile_app_kotlin.activities.LoginActivity
import com.example.yestech_mobile_app_kotlin.utils.UserSessionEducator
import com.example.yestech_mobile_app_kotlin.utils.UserSessionStudent

class MainActivity : AppCompatActivity() {

    private var context: Context? = null
    private var navigation: BottomNavigationView? = null
    private var role: String? = null

    private var fragmentManager: FragmentManager? = null

    private var currentFragment = "1"

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    currentFragment = "1"
                    role?.let { openHomeFragment(it) }
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_chat -> {
                    currentFragment = "2"
                    openChatFragment()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_quiz -> {
                    currentFragment = "3"
                    role?.let { openQuizFragment(it) }
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_notification -> {
                    currentFragment = "4"
                    openNotificationFragment()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_menu -> {
                    currentFragment = "5"
                    openMenuFragment()
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation = findViewById(R.id.navigation) as BottomNavigationView
        navigation!!.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        context = this
        checkUserSession()
    }

    override fun onBackPressed() {
        if (currentFragment != "1") {
            currentFragment = "1"
            role?.let { openHomeFragment(it) }
            navigation.setSelectedItemId(R.id.navigation_home)
        } else {
            super.onBackPressed()
            finish()
        }

    }


    private fun openHomeFragment(role: String) {
        title = "Home"
        val bundle = Bundle()
        bundle.putString("ROLE", role)
        val homeFragment = HomeFragment()
        homeFragment.setArguments(bundle)
        fragmentManager = supportFragmentManager
        fragmentManager.beginTransaction().replace(R.id.container, homeFragment, homeFragment.getTag()).commit()
    }

    private fun openChatFragment() {
        title = "Chat"
        val chatFragment = ChatFragment()
        fragmentManager = supportFragmentManager
        fragmentManager.beginTransaction().replace(R.id.container, chatFragment, chatFragment.getTag()).commit()
    }

    private fun openQuizFragment(role: String) {
        title = "Quiz"
        val bundle = Bundle()
        bundle.putString("ROLE", role)
        val quizFragment = QuizFragment()
        quizFragment.setArguments(bundle)
        fragmentManager = supportFragmentManager
        fragmentManager.beginTransaction().replace(R.id.container, quizFragment, quizFragment.getTag()).commit()
    }

    private fun openNotificationFragment() {
        title = "Notification"
        val notificationFragment = NotificationFragment()
        fragmentManager = supportFragmentManager
        fragmentManager.beginTransaction().replace(R.id.container, notificationFragment, notificationFragment.getTag())
            .commit()
    }

    private fun openMenuFragment() {
        title = "Menu"
        val menuFragment = MenuFragment()
        fragmentManager = supportFragmentManager
        fragmentManager.beginTransaction().replace(R.id.container, menuFragment, menuFragment.getTag()).commit()
    }

    fun checkUserSession() {

        val studToken = context?.let { UserSessionStudent().getToken(it) }
        val teachToken = UserSessionEducator.getToken(context)

        if (studToken == "" && teachToken == "") {
            val login = Intent(this@MainActivity, LoginActivity::class.java)
            startActivity(login)
            finish()
        } else {
            if (studToken != "") {
                role = "STUDENT"
                openHomeFragment(role)
            }
            if (teachToken != "") {
                role = "EDUCATOR"
                openHomeFragment(role)
            }
        }
    }

}
