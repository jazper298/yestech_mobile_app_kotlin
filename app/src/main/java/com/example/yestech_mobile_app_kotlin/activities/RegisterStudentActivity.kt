package com.example.yestech_mobile_app_kotlin.activities

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.example.yestech_mobile_app_kotlin.R
import com.example.yestech_mobile_app_kotlin.utils.Debugger
import com.example.yestech_mobile_app_kotlin.utils.HttpProvider
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_register_student.*
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset

class RegisterStudentActivity : AppCompatActivity() {

    lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_student)

        context= this@RegisterStudentActivity

    }

    override fun onStart() {
        super.onStart()
        initializeUI()
    }

    private fun initializeUI(){
        var btnNext = findViewById<Button>(R.id.btn_RegDone)
        var et_Email = findViewById<EditText>(R.id.et_Email)
        var et_Password = findViewById<View>(R.id.et_Password) as EditText
        var et_ConfirmPassword = findViewById<View>(R.id.et_ConfirmPassword) as EditText

        btnNext.setOnClickListener {
            if (fieldsAreEmpty()) {
                Toasty.warning(this, "Please input all fields.").show()
                et_Email.requestFocus()
            } else if (et_Password.text.toString() == et_ConfirmPassword.text.toString()) {
                registerStudent()
            } else {
                Toasty.error(this, "Password didn't match!").show()
            }
        }
    }

    private fun fieldsAreEmpty(): Boolean {
        return et_Email.getText().toString().isEmpty() && et_Password.getText().toString().isEmpty() && et_ConfirmPassword.getText().toString().isEmpty()
    }

    private fun registerStudent() {
        try {

            val progressDialog = ProgressDialog(context)

            val params = RequestParams()
            params.put("s_email_address", et_Email.text.toString())
            params.put("s_password", et_ConfirmPassword.text.toString())

            val stringEntity = StringEntity(params.toString())

            HttpProvider().post(applicationContext, "controller_student/register_as_student_class.php/", stringEntity, object :
                AsyncHttpResponseHandler() {

                override fun onStart() {
                    super.onStart()
                    progressDialog.show()
                }

                override fun onFinish() {
                    super.onFinish()
                    progressDialog.dismiss()
                }

                override fun onSuccess(statusCode: Int, headers: Array<Header>, responseBody: ByteArray) {
                    Debugger().logD(responseBody.toString())

                    try {
                        val charset: Charset = Charsets.UTF_8

                        val str = String(responseBody, charset)
                        Debugger().logD(str)
                        Toasty.success(applicationContext, "Successfully Registered, Please check your email for verification.")
                            .show()
                    } catch (e: UnsupportedEncodingException) {
                        e.printStackTrace()
                    }

                    val mainintent = Intent(applicationContext, LoginActivity::class.java)
                    mainintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(mainintent)
                    finish()
                }

                override fun onFailure(statusCode: Int, headers: Array<Header>, responseBody: ByteArray, error: Throwable) {
                    Toasty.error(applicationContext, "Error From Server 1").show()
                }

            })

        } catch (err: Exception) {
            Debugger().logD("YES " + err.toString());
            Toasty.error(applicationContext, err.toString()).show()
        }

    }

}
