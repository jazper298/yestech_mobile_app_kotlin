package com.example.yestech_mobile_app_kotlin.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import com.example.yestech_mobile_app_kotlin.R
import com.example.yestech_mobile_app_kotlin.adapters.LoginPageAdapter
import com.example.yestech_mobile_app_kotlin.fragments.LoginStudentFragment
import com.example.yestech_mobile_app_kotlin.fragments.LoginTeacherFragment

class LoginActivity : AppCompatActivity() {

    private var context = null
    private var view = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        context= this@LoginActivity.currentFocus as Nothing?

        view = this@LoginActivity.currentFocus as Nothing?

        initializeUI()
    }
    private fun initializeUI(){

        val viewPager = findViewById<ViewPager>(R.id.vpLogin)
        val tabLayout = findViewById<TabLayout>(R.id.tabLayoutLogin)
        val adapter = LoginPageAdapter(supportFragmentManager)
        adapter.addFragment(LoginStudentFragment().newInstance(), "S t u d e n t ")
        adapter.addFragment(LoginTeacherFragment().newInstance(), "E d u c a t o r ")

        viewPager.adapter  = adapter
        tabLayout.setupWithViewPager(viewPager)
    }
}
