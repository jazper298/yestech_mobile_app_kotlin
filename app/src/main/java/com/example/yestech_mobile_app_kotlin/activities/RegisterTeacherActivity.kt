package com.example.yestech_mobile_app_kotlin.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.yestech_mobile_app_kotlin.R

class RegisterTeacherActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_teacher)
    }
}
