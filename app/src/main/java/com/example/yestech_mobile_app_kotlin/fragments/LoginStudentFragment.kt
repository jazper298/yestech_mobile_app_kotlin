package com.example.yestech_mobile_app_kotlin.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView

import com.example.yestech_mobile_app_kotlin.R
import com.example.yestech_mobile_app_kotlin.activities.RegisterStudentActivity
import es.dmoral.toasty.Toasty

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [LoginStudentFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [LoginStudentFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class LoginStudentFragment : Fragment() {
    //private var view
    private var context = null
    //private var view = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view =  inflater.inflate(R.layout.fragment_login_student, container, false)
//        context= getContext() as Nothing?
        //initializeUI()
        return view
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var tvCreateAccount = view.findViewById<TextView>(R.id.tv_CreateAccount)
        tvCreateAccount.setOnClickListener {
            //Toasty.error(getContext()!!, "Password didn't match!").show()
            val intent = Intent(getContext(), RegisterStudentActivity::class.java)
            startActivity(intent)
        }
    }

    fun newInstance(): LoginStudentFragment {
        val fragment = LoginStudentFragment()
        val args = Bundle()
        fragment.arguments = args
        return fragment
    }
}
