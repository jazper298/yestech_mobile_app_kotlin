package com.example.yestech_mobile_app_kotlin.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.yestech_mobile_app_kotlin.R

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [LoginTeacherFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [LoginTeacherFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class LoginTeacherFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_login_teacher, container, false)

        return view
    }

    fun newInstance(): LoginStudentFragment {
        val fragment = LoginStudentFragment()
        val args = Bundle()
        fragment.arguments = args
        return fragment
    }
}
